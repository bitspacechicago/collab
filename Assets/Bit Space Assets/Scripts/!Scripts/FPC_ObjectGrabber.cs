﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FPC_ObjectGrabber : MonoBehaviour {

	// whether the player has successfully activated something
	// initially false, but other scripts change this value
	private bool occupied;

	[HideInInspector]
	public FPC_GrabbableObject currentlyGrabbedObject = null;
	public KeyCode grabButton = KeyCode.Mouse0;

	public Transform raycastSource;
	public LayerMask layersToHit;
	public Image playerReticle;
	public Text playerInstructions;

	public float interactDistance;


	public void setBusy() {
		occupied = true;
		if (playerReticle != null) {
			playerReticle.enabled = false;
		}
		if (playerInstructions != null) {
			playerInstructions.enabled = false;
		}
	}

	public void setFree() {
		occupied = false;
		if (playerReticle != null) {
			playerReticle.enabled = true;
		}
		if (playerInstructions != null) {
			playerInstructions.enabled = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (occupied == false) {
			if (Input.GetKeyDown(grabButton)) {
				AttemptGrab ();
			}
		}
	}

	public void AttemptGrab() {
		RaycastHit hit;

		Debug.DrawRay (raycastSource.position, raycastSource.forward * interactDistance, Color.blue, 2f);
		if (Physics.Raycast (raycastSource.position, raycastSource.forward, out hit, interactDistance)) {//, layersToHit)) {
			Debug.Log (hit.transform.name);
			if (hit.rigidbody != null && hit.rigidbody.GetComponent<FPC_GrabbableObject>() != null) {
				hit.rigidbody.GetComponent<FPC_GrabbableObject>().GetGrabbedBy(this);
			}
			//hit.transform.SendMessageUpwards ("ActivateObject", SendMessageOptions.DontRequireReceiver);
		}
	}

	public void DropItem() {
		if (this.currentlyGrabbedObject != null) {
			currentlyGrabbedObject.DropMe ();
		}
	}
}
