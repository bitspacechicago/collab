﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ScaleTransforms : MonoBehaviour {
	public Transform[] thingsToScale;
	private Vector3[] originalScales;
	public Vector3[] newScales;

	public UnityEvent scaleEvents;
	public UnityEvent unscaleEvents;

	void Awake() {
		originalScales = new Vector3[thingsToScale.Length];
		for (int i = 0; i < thingsToScale.Length; i++) {
			originalScales[i] = thingsToScale[i].localScale;
		}
	}

	public void Scale() {
		scaleEvents.Invoke ();
		for (int i = 0; i < thingsToScale.Length; i++) {
			thingsToScale[i].localScale = newScales[i];
		}

	}

	public void UnScale() {
		unscaleEvents.Invoke ();
		for (int i = 0; i < thingsToScale.Length; i++) {
			thingsToScale[i].localScale = originalScales [i];
		}
	}


}
