﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class LauncherWithVariance : MonoBehaviour {
	public KeyCode fireKey = KeyCode.Mouse0;

	public GameObject projectilePrefab;
	public Transform launchPoint;
	public bool autoFire = false;
	[Tooltip("Projectile always fires along local Z+ direction of Launch Point")]
	public float launchVelocity = 5f;
	public float launchVelocityVariance = 0f;
	public float launchXVariance = 0f;
	public float launchYVariance = 0f;
	public float rotationVariance = 0f;
	public float angularVelocityVariance = 0f;

	public float projectilesPerSecond = 1f;
	public float projectileLifetime = 3f;
	private float _lastFireTime = 0f;

	[Header("If checked, ignore all ammo settings below")]
	public bool infiniteAmmo = true;
	public int startingAmmo = 10;
	public int maxAmmo = 10;
	public int currentAmmo;
	public float ammoRechargeTime = 1f;
	private float _lastAmmoRechargeTime = 0f;
	public IconCounterHUD ammoCounter;

	public UnityEvent launchEvents;


	void Start() {
		this.SetAmmo (startingAmmo);
	}

	void Update() {
		if (autoFire || Input.GetKey (fireKey))
		{
			Launch();
		}

		if (ammoRechargeTime > 0f && Time.time > _lastAmmoRechargeTime + ammoRechargeTime) {
			this.AddAmmo(1);
			_lastAmmoRechargeTime = Time.time;
		}
	}

	public void Launch() {
		if (!this.enabled) {
			return;
		}

		if (currentAmmo > 0f  && Time.time - _lastFireTime >= 1f / projectilesPerSecond)
		{
			_lastFireTime = Time.time;

			// ignore removing ammo if it's infinite
			if (!infiniteAmmo) {
				AddAmmo (-1);
			}

			GameObject newGO;
			if (launchPoint != null)
			{
				newGO = GameObject.Instantiate (projectilePrefab, launchPoint.position, Quaternion.RotateTowards (launchPoint.rotation, Random.rotationUniform, rotationVariance) ) as GameObject;
			}
			else
			{
				newGO = GameObject.Instantiate (projectilePrefab, this.transform.position, Quaternion.RotateTowards (this.transform.rotation, Random.rotationUniform, rotationVariance) ) as GameObject;
			}

			Rigidbody newRB = newGO.GetComponent<Rigidbody> ();

			if (newRB != null)
			{
				Vector2 unitCircle = Random.insideUnitCircle;
				Vector3 launchVector = new Vector3 (unitCircle.x * launchXVariance, unitCircle.y * launchYVariance, launchVelocity + launchVelocityVariance * (Random.value - Random.value));
				newRB.AddRelativeForce (launchVector, ForceMode.VelocityChange);
				newRB.AddRelativeTorque (Random.onUnitSphere * angularVelocityVariance, ForceMode.VelocityChange);
			}
			if (projectileLifetime > 0f) {
				GameObject.Destroy(newGO, projectileLifetime);
			}

			launchEvents.Invoke ();
		}
	}

	public void AddAmmo(int amount) {
		SetAmmo (currentAmmo + amount);
	}

	public void SetAmmo( int amount) {
		currentAmmo = Mathf.Clamp (amount, 0, maxAmmo);
		if (ammoCounter != null) {
			ammoCounter.SetCount (currentAmmo);
		}
	}
}
