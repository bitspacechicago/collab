﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayRandomAudioClip : MonoBehaviour {
	public bool playOnEnable = true;
	public bool stopOnDisable = true;
	public AudioSource audioSource;
	public List<AudioClip> audioClips = new List<AudioClip>();

	public void PlayRandomClip() {
		if (EverythingChecksOut()) {
			audioSource.clip = audioClips [Random.Range (0, audioClips.Count)];
			audioSource.Play ();
		}
	}

	public void PlaySpecificClip(int clip) {
		if (clip < 0) {
			Debug.LogError ("Can't play negative numbered clip!");
			return;
		}

		if (clip >= audioClips.Count) {
			Debug.LogError ("Can't play clip #" + clip.ToString() + " because it doesn't exist!");
			return;
		}

		if (EverythingChecksOut()) {
			audioSource.clip = audioClips [clip];
			audioSource.Play ();
		}
	}

	public void Stop() {
		if (EverythingChecksOut()) {
			audioSource.Stop ();
		}
	}

	void OnEnable() {
		if (playOnEnable && EverythingChecksOut()) {
			audioSource.clip = audioClips [Random.Range (0, audioClips.Count)];
			audioSource.Play ();
		}
	}
	void OnDisable() {
		if (stopOnDisable && EverythingChecksOut()) {
			audioSource.Stop ();
		}
	}

	private bool EverythingChecksOut() {
		if (audioSource == null) {
			Debug.LogError ("Audio Source not set on " + this.gameObject.name);
			return false;
		}

		if (audioClips.Count <= 0) {
			Debug.LogError("No Audio Clips set on " + this.gameObject.name);
			return false;
		}

		return true;
	}
}
