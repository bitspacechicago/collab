﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Utility;
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(WaypointProgressTracker))]
public class WaypointCircuitForceFollower : MonoBehaviour {
	private Transform followPoint;
	public float forwardForce = 10f;
	public float turnTorque = 5f;
	public float selfRightingForce = 1f;
	private Rigidbody _rb;

	// temporary variables
	private Vector3 diffV;
	private Vector3 crossAxis;
	private float angle;

	void Awake() {
		_rb = this.GetComponent<Rigidbody> ();
	}


	// Update is called once per frame
	void FixedUpdate () {
		// we will force the user to use the followpoint set int the Waypoint Progress Tracker
		if (followPoint == null) {
			if (this.GetComponent<WaypointProgressTracker> ().target != null) {
				// this might get instantiated in the first update -- so we wait for the to happen
				followPoint = this.GetComponent<WaypointProgressTracker> ().target;
			} else {
				// otherwise we just bail
				return;
			}
		}

		// turning towards the circuit
		diffV = followPoint.position - this.transform.position;
		angle = Vector3.Angle (this.transform.forward, diffV);
		crossAxis = Vector3.Cross (this.transform.forward, diffV);

		_rb.AddTorque (crossAxis * angle * turnTorque, ForceMode.Acceleration);

		// turning ourselves upright
		angle = Vector3.Angle (this.transform.up, Vector3.up);
		crossAxis = Vector3.Cross (this.transform.up, Vector3.up);

		_rb.AddTorque (crossAxis * angle * selfRightingForce, ForceMode.Acceleration);


		// pushing ourselves forwards
		_rb.AddForce (this.transform.forward * forwardForce, ForceMode.Acceleration);
	}

	public void SetSpeed(float newForce) {
		forwardForce = newForce;
	}

	public void SetTurnSpeed(float newTorque) {
		turnTorque = newTorque;
	}
}
