﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpringJointVisuals : MonoBehaviour {
	private List<SpringJoint> joints;
	public List<LineRenderer> renderers = new List<LineRenderer>();
	// Use this for initialization
	void Awake () {
		joints = new List<SpringJoint>(this.GetComponents<SpringJoint>());
		if(joints.Count != renderers.Count) {
			Debug.LogError ("Joints and renderer count is not the same!");
			this.enabled = false;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		for (int i = 0; i < joints.Count; i++) {
			renderers [i].SetPositions (new Vector3[] {
				this.transform.TransformPoint (joints [i].anchor),
				joints [i].connectedBody.transform.TransformPoint (joints [i].connectedAnchor)
			});
		}
	}
}
