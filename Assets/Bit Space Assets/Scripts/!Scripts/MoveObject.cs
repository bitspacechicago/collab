﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObject : MonoBehaviour {
	public Transform newPosition;
	public bool moveInWorldSpace = false;
	private Transform originalPosition;
	public float moveTime = 1f;

	void Awake () {
		GameObject savedGO = new GameObject(this.gameObject.name + " - original position");
		originalPosition = savedGO.transform;

		if (moveInWorldSpace) {
			originalPosition.SetParent(null);
			originalPosition.position = this.transform.position;
			originalPosition.rotation = this.transform.rotation;
			originalPosition.localScale = this.transform.localScale;
		} else {
			originalPosition.SetParent(this.transform.parent);
			originalPosition.localPosition = this.transform.localPosition;
			originalPosition.localRotation = this.transform.localRotation;
			originalPosition.localScale = this.transform.localScale;
		}

	}
	
	public void MoveThere() {
		this.StopAllCoroutines();
		StartCoroutine(MoveRoutine(newPosition));
	}

	private IEnumerator MoveRoutine(Transform target) {
		yield return new WaitForFixedUpdate();

		float endTime = Time.fixedTime + moveTime;
		float percent;

		Vector3 startPos;
		Quaternion startRot;
		Vector3 startScale;

		if (moveInWorldSpace) {
			startPos = this.transform.position;
			startRot = this.transform.rotation;
			startScale = this.transform.localScale;
		} else {
			startPos = this.transform.localPosition;
			startRot = this.transform.localRotation;
			startScale = this.transform.localScale;
		}

		while (Time.fixedTime < endTime) {
			
			percent = 1 - (endTime - Time.fixedTime) / moveTime;
			if (moveInWorldSpace) {
				
				this.transform.position = Vector3.Lerp(startPos, target.position, percent);
				this.transform.rotation = Quaternion.Slerp(startRot, target.rotation, percent);
				this.transform.localScale = Vector3.Lerp(startScale, target.localScale, percent);
			} else {
				this.transform.localPosition = Vector3.Lerp(startPos, target.localPosition, percent);
				this.transform.localRotation = Quaternion.Slerp(startRot, target.localRotation, percent);
				this.transform.localScale = Vector3.Lerp(startScale, target.localScale, percent);

			}
			yield return new WaitForFixedUpdate();
		}

		if (moveInWorldSpace) {

			this.transform.position = target.position;
			this.transform.rotation = target.rotation;
			this.transform.localScale = target.localScale;
		} else {
			this.transform.localPosition = target.localPosition;
			this.transform.localRotation = target.localRotation;
			this.transform.localScale = target.localScale;
		}


	}

	public void MoveBack() {
		this.StopAllCoroutines();
		StartCoroutine(MoveRoutine(originalPosition));
	}
}
