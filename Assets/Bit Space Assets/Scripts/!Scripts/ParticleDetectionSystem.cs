﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleDetectionSystem : MonoBehaviour {
	public Color lowDetectionColor = Color.white;
	public Color highDetectionColor = Color.red;

	public float alertnessPerParticle = 1f;
	public float alarmThreshold = 10f;
	public float alertnessLossPerSecond = 2f;
	public float currentAlertness = 0f;

	public enum AlertnessState {
		Zero, Alerted, Alarm
	}

	public UnityEvent detectionEvents;

	public AlertnessState alertnessState = AlertnessState.Zero;


	public UnityEvent alertedEvents;
	public UnityEvent alarmedEvents;
	public UnityEvent resetEvents;

	private ParticleSystem ps;


	private List<ParticleSystem.Particle> enterParts = new List<ParticleSystem.Particle> ();

	// Use this for initialization
	void Awake () {
		this.ps = this.GetComponent<ParticleSystem> ();
	}

	void LateUpdate() {
		if (alertnessState < AlertnessState.Alarm) {
			this.currentAlertness = Mathf.Clamp (currentAlertness - alertnessLossPerSecond * Time.deltaTime, 0f, alarmThreshold);
		}

		if (currentAlertness == 0f && alertnessState > AlertnessState.Zero) {
			alertnessState = AlertnessState.Zero;
			resetEvents.Invoke ();
		} else if (currentAlertness > 0f && alertnessState == AlertnessState.Zero) {
			alertnessState = AlertnessState.Alerted;
			alertedEvents.Invoke ();
		} else if (currentAlertness >= alarmThreshold) {
			alertnessState = AlertnessState.Alarm;
			alarmedEvents.Invoke ();
		}
	}

	// Update is called once per frame
	void OnParticleTrigger () {
		int numEnter = ps.GetTriggerParticles (ParticleSystemTriggerEventType.Enter, enterParts);

		for (int i = 0; i < numEnter; i++) {
			detectionEvents.Invoke ();

			currentAlertness += alertnessPerParticle;
			ParticleSystem.Particle p = enterParts [i];
			p.startColor = Color.Lerp (lowDetectionColor, highDetectionColor, currentAlertness / alarmThreshold);
			enterParts [i] = p;
		}

		ps.SetTriggerParticles (ParticleSystemTriggerEventType.Enter, enterParts);
	}

	public void Reset() {
		if (alertnessState > AlertnessState.Zero) {
			resetEvents.Invoke ();
		}

		alertnessState = AlertnessState.Zero;
		currentAlertness = 0f;
	}
}
