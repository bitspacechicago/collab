﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))]
// Handles the equipping and unequipping of an item
public class FPC_GrabbableObject : MonoBehaviour {

	private Rigidbody _myRB;
	private bool RBWasKinematic = false;
	public bool makeNotKinematicOnRelease = false;
	// where the item will be equipped to
	// must be a child of a Camera
	public string equipPointName;

	private Transform equipPointTransform;
	// reference to the activating script
	private FPC_ObjectGrabber myGrabber;
	// reference to our instruction Text component
	public Text instructions;
	// key to drop item
	public KeyCode dropItemKey = KeyCode.X;

	// status and timing flags
	private bool itemEquipping;
	private bool itemReady;

	// adjustable constants
	public float equipTime = 0.5f;
	public float unequipTime = 0.5f;
	public float floatHeight = 0.1f;

	// hierarchy information
	public bool restorePreviousParent = false;
	private Transform itemPreviousParent;
	private Transform camParent;


	// Use this for initialization
	void Start () {
		this._myRB = this.GetComponent<Rigidbody>();
		this.RBWasKinematic = _myRB.isKinematic;

		itemEquipping = false;
		itemReady = false;
		if (instructions != null) {
			instructions.enabled = false;
		}
		itemPreviousParent = this.transform.parent;
	}

	// Update is called once per frame
	void Update () {

		if (itemReady) {
			// 
			if (Input.GetKeyDown (dropItemKey)) {
				itemReady = false;
				StartCoroutine(DropItem());
				// don't allow anything to fire if we're dropping the item
				return;
			}
		}

	}

	public void GetGrabbedBy(FPC_ObjectGrabber grabber) {
		if (itemEquipping == false && itemReady == false) {
			myGrabber = grabber;
			myGrabber.currentlyGrabbedObject = this;
			StartCoroutine(MoveAndPrepItem(grabber));
		}
	}

	IEnumerator MoveAndPrepItem(FPC_ObjectGrabber grabber) {

		if (instructions != null) {
			instructions.enabled = true;
		}

		grabber.setBusy ();

		_myRB.isKinematic = true;

		if (equipPointName == "") {
			equipPointTransform = grabber.raycastSource;
		} else {
			equipPointTransform = grabber.raycastSource.Find (equipPointName);
		}

		if (equipPointTransform == null) {
			Debug.LogError("Equip Point Named " + equipPointName + " not found on FPC Object Grabber " + grabber.name);

		} else {


			float endTime = Time.time + equipTime;
			float percent;
			while (Time.time <= endTime) {
				percent = 1 - (endTime - Time.time) / equipTime;
				this.transform.position = Vector3.Lerp(this.transform.position, equipPointTransform.position, percent);
				this.transform.rotation = Quaternion.Slerp(this.transform.rotation, equipPointTransform.rotation, percent);
				yield return new WaitForFixedUpdate();
			}
			this.transform.parent = equipPointTransform;

			itemReady = true;
		}
	}

	public void DropMe() {
		StartCoroutine(DropItem());
	}

	IEnumerator DropItem() {
		if (myGrabber == null) {
			yield break;
		}

		if (makeNotKinematicOnRelease) {
			_myRB.isKinematic = false;
		} else {
			_myRB.isKinematic = RBWasKinematic;
		}

		if (instructions != null) {
			instructions.enabled = false;
		}

		itemReady = false;

		if (restorePreviousParent) {
			this.transform.parent = itemPreviousParent;
		} else {
			this.transform.parent = null;
		}

		float endTime = Time.time + unequipTime;
		float percent;
		// set up the destination point
		RaycastHit hit;

		Vector3 destPos;
		// if we try to put it into the ground ...
		if (Physics.Raycast (myGrabber.raycastSource.position, myGrabber.raycastSource.forward, out hit, myGrabber.interactDistance + 0.5f, Physics.DefaultRaycastLayers ,QueryTriggerInteraction.Ignore)) {
			// float it above ground
			destPos = hit.point + Vector3.up * floatHeight;
		} else {
			// if we try to put it in the air, move it away from us, then drop it to floatHeight
			destPos = this.transform.position + equipPointTransform.forward * myGrabber.interactDistance * 0.8f;
			if (Physics.Raycast (destPos, Vector3.down, out hit)) {
				destPos = hit.point + Vector3.up * floatHeight;
			}
		}
		Quaternion destRot = Quaternion.AngleAxis (equipPointTransform.eulerAngles.y, Vector3.up);

		while(Time.time <= endTime) {
			percent = 1 - (endTime - Time.time) / unequipTime;
			this.transform.position = Vector3.Lerp (this.transform.position, destPos, percent);
			this.transform.rotation = Quaternion.Slerp (this.transform.rotation, destRot, percent);
			yield return new WaitForFixedUpdate();
		}


		LetGo();
	}

	void OnDestroy() {
		this.LetGo();
	}

	private void LetGo() {
		itemReady = false;
		itemEquipping = false;
		if (myGrabber != null) {
			myGrabber.setFree();
			myGrabber = null;
			myGrabber.currentlyGrabbedObject = null;
		}
	}
}
