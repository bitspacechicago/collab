﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ProgressBarHUD : MonoBehaviour {
	public RectTransform progressBar;
	public enum ProgressAxis {
		X, Y
	};

	public ProgressAxis progressAxis;

	[Range(0f, 1f)]
	public float progress;

	public bool lockAfterFinish = false;
	private bool finishLocked = false;
	private bool recentlyFinished = false;

	public bool changeColors = false;
	public Gradient progressGradient;

	public UnityEvent finishEvents;

	public void ResetProgress() {
		recentlyFinished = false;
		SetProgress (0f);
	}

	public void ResetAndUnlock() {
		finishLocked = false;
		ResetProgress ();
	}

	public void AddProgress(float percent) {
		SetProgress (progress + percent);
	}

	public void SetProgress(float newProgress) {
		progress = Mathf.Clamp01 (newProgress);

		UpdateVisuals ();

		if (progress >= 1f && !recentlyFinished && !finishLocked) {
			recentlyFinished = true;
			if (lockAfterFinish) {
				finishLocked = true;
			}
			finishEvents.Invoke ();
		}
	}

	private void UpdateVisuals() {
		Vector3 newScale = progressBar.localScale;
		newScale [(int) progressAxis] = progress;
		progressBar.localScale = newScale;

		if (changeColors && progressBar.GetComponent<Image>() != null) {
			progressBar.GetComponent<Image> ().color = progressGradient.Evaluate (progress);
		}
	}

	void OnValidate() {
		this.UpdateVisuals ();
	}
}
