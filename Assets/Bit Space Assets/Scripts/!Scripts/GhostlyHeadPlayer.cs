﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GhostlyHeadPlayer : MonoBehaviour {
	public GameObject cameraRig;
	public Camera cam;
	private Transform cam_t;
	private Rigidbody rb;

	public float forwardSpeed = 3f;
	public float backwardSpeed = 1f;
	public float strafeSpeed = 1f;

	public string leftRightAxis = "Horizontal";
	public string upDownAxis = "Vertical";
	public string forwardBackAxis = "Triggers";
	public string turnLRAxis = "JoystickAxis4";
	public string turnUPAxis = "JoystickAxis5";

	public float turnSpeed = 1f;


	// Use this for initialization
	void Start () {
		
		rb = this.GetComponent<Rigidbody> ();
		cam_t = cam.GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float LR = Input.GetAxisRaw (leftRightAxis);
		float UD = Input.GetAxisRaw (upDownAxis);
		float FB = Input.GetAxisRaw (forwardBackAxis);

		float turnLR = Input.GetAxis (turnLRAxis);
		float turnUD = Input.GetAxis (turnUPAxis);

		cameraRig.transform.position = rb.position;

//		Debug.Log("turnUD: " + turnUD + ", turnLR: " + turnLR);

		cam_t.localEulerAngles = new Vector3 (turnUD * turnSpeed + cam_t.localEulerAngles.x, turnLR * turnSpeed + cam_t.localEulerAngles.y, 0f);

		Vector3 moveDir = cam_t.forward * FB * forwardSpeed + cam_t.right * LR * strafeSpeed + Vector3.up * UD * strafeSpeed;
		rb.MovePosition (rb.position + moveDir * Time.fixedDeltaTime);
		rb.MoveRotation (cam_t.rotation);
	}
}
