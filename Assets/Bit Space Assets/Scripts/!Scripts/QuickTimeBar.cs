﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class QuickTimeBar : MonoBehaviour {
	public ProgressBarHUD progressBar;
	public Image targetRangeIndicator;
	public Color indicatorUnderColor = Color.black;
	public Color indicatorSuccessColor = Color.green;
	public Color indicatorOverColor = Color.red;

	public bool active = false;

	public float progressPerSecond = 0.1f;


	[Range(0f, 1f)]
	public float minSuccessValue = 0.5f;
	[Range(0f, 1f)]
	public float maxSuccessValue = 0.7f;

	public bool resetProgressAfterTrigger = true;

	public UnityEvent startEvents;
	public UnityEvent successEvents;
	public UnityEvent failureEvents;


	// Use this for initialization
	void Start () {
		progressBar.ResetProgress ();
	}
	
	// Update is called once per frame
	void Update () {
		if (active) {
			progressBar.AddProgress (progressPerSecond * Time.deltaTime);
		}

		if (progressBar.progress < minSuccessValue) {
			targetRangeIndicator.color = indicatorUnderColor;
		} else if (progressBar.progress > maxSuccessValue) {
				targetRangeIndicator.color = indicatorOverColor;
			} else {
				targetRangeIndicator.color = indicatorSuccessColor;
			}
	}

	public void Trigger() {
		if (active) {
			EvaluateQuickTime ();
		} else {
			StartQuickTime ();
		}
	}

	public void StartQuickTime() {
		if (!this.enabled) {
			return;
		}

		progressBar.ResetProgress ();

		this.active = true;

		startEvents.Invoke ();
	}

	public void EvaluateQuickTime() {
		if (!this.enabled) {
			return;
		}

		this.active = false;

		// evaluate the progress, give success or loss messages
		if (progressBar.progress >= minSuccessValue && progressBar.progress <= maxSuccessValue) {
			successEvents.Invoke ();
		} else {
			failureEvents.Invoke ();
		}

		if (resetProgressAfterTrigger) {
			progressBar.ResetProgress ();
		}

	}

	void OnValidate() {
		Vector2 newAnchor = targetRangeIndicator.rectTransform.anchorMin;

		newAnchor [(int)progressBar.progressAxis] = minSuccessValue;
		targetRangeIndicator.rectTransform.anchorMin = newAnchor;
			
		newAnchor = targetRangeIndicator.rectTransform.anchorMax;
		newAnchor [(int)progressBar.progressAxis] = maxSuccessValue;
		targetRangeIndicator.rectTransform.anchorMax = newAnchor;
	}
}
